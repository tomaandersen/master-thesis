from mne_realtime import LSLClient
from threading import Thread
import logging


class RealTimeClient:

    def __init__(self, info, host='Smarting', wait_max=5, n_samples=150, verbose=False):
        self._lsl_client = LSLClient(info=info, host=host, wait_max=wait_max, verbose=verbose)
        self._epoch_samples = n_samples
        self._recv_callbacks = list()
        self._recv_thread = None
        self._current_epoch_index = 0

    def start(self):
        self._lsl_client.start()

        # Start epoch receiver daemon thread.
        self._start_epoch_received_thread()

    def stop(self):
        self._lsl_client.stop()

    def __enter__(self):
        self.start()

    def __exit__(self):
        self.stop()

    def _start_epoch_received_thread(self):
        if self._recv_thread is None:
            self._recv_thread = Thread(target=self._recv_epoch_worker)
            self._recv_thread.daemon = True
            self._recv_thread.start()

    def _recv_epoch_worker(self):
        while True:
            new_epoch = self._lsl_client.get_data_as_epoch(self._epoch_samples)
            for callback in self._recv_callbacks:
                callback(new_epoch, self._current_epoch_index)
            self._current_epoch_index += 1

    def register_epoch_received_callback(self, callback):
        if callback not in self._recv_callbacks:
            self._recv_callbacks.append(callback)

    def unregister_epoch_received_callback(self, callback):
        if callback in self._recv_callbacks:
            self._recv_callbacks.remove(callback)
