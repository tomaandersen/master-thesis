#!python3

import os
import time
import sys
import numpy as np
from multiprocessing import Queue
from pylsl import StreamInfo, StreamOutlet
import keyboard
from datetime import datetime
from threading import Lock
import random

import matplotlib.pyplot as plt
import matplotlib as mpl

# plt.imshow(mpl.image.imread('frida.jpg'))
# plt.pause(0.5)

# Use this to run program
# sudo python3 run_stimuli_eyes.py

# To write to log file without conflict. 
log_lock = Lock()

clear_command = 'cls'
# Colors for text
BASE_COLOR = "\033[38;5;7m"  # Default color
RED = "\033[38;5;196m"
GREEN = "\033[38;5;120m"

# global variables that decides state of prgoram
is_paused = True  # Program starts as if it were paused.
is_closing = False
time_last_stimuli = time.time()
next_stimuli = "+"
time_next_stimuli = 0
nbr_stim = {'left': 0,
            'right': 0,
            'up': 0,
            'down': 0,
            'tot': 0}

# The markers to send to lsl to show what is shown on the screen.
markers = {'+': 0000,
           'left': 1001,
           'right': 2001,
           'up': 3001,
           'down': 4001,
           'start_pause': 8001,
           'end_pause': 8002,
           'end_trial': 9999}


# ---- Print to screen .-------
def welcome():
  '''
  Text in beginning of trial.
  '''
  os.system(clear_command)
  print("\033[?25l")
  print("Welcome to the left_right_arm_experiment.")
  print("Try to" + RED + " sit still and not move your eyes" + BASE_COLOR)
  print("Keep your eyes at the + when it is visible.")
  print("You can pause the trial anytime by pressing space.")
  print("When the trial is done you quit by pressing Esc or q.")
  print("")
  print("Wait for trail to be ready.")
  input('\nPress enter to continue.')


def print_text_mid_screen(text):
  '''
  Function to print text in the middle of the screen
  Input:
  text : str - text to print

  Returns:
  Prints text to screen.
  '''
  os.system(clear_command)
  print("\033[12B{:^80}".format(text))


def print_text_top_screen(text):
  '''
  Function to print text in the middle of the screen
  Input:
  text : str - text to print

  Returns:
  Prints text to screen.
  '''
  os.system(clear_command)
  print("{:^80}".format(text))


def print_text_left_screen(text):
  '''
  Function to print text in the middle of the screen
  Input:
  text : str - text to print

  Returns:
  Prints text to screen.
  '''
  os.system(clear_command)
  print("\033[12B{:<80}".format(text))


def print_text_right_screen(text):
  '''
  Function to print text in the middle of the screen
  Input:
  text : str - text to print

  Returns:
  Prints text to screen.
  '''
  os.system(clear_command)
  print("\033[12B{:>80}".format(text))


def print_text_bottom_screen(text):
  '''
  Function to print text in the middle of the screen
  Input:
  text : str - text to print

  Returns:
  Prints text to screen.
  '''
  os.system(clear_command)
  print("\033[22B{:^80}".format(text))


# ----- Button pressed -------
def q_is_pressed():
  '''
What happens when user presses q - for closing program.
'''
  global is_closing
  marker = markers['end_trial']
  outlet.push_sample([marker])
  print_text_mid_screen('Trials closing. Thank you!')
  write_to_log(time.time(), 'Quitting', marker)
  is_closing = True


def space_is_pressed():
  '''
What happens when user presses space bar - for pausing and resuming.
'''
  global is_paused, next_stimuli
  if is_paused == True:  # user resumes.
    marker = markers['end_pause']
    outlet.push_sample([marker])
    is_paused = False
    print_text_mid_screen('+')
    next_stimuli = "+"
    write_to_log(time.time(), 'Resumes', marker)

  else:  # user pauses
    marker = markers['start_pause']
    outlet.push_sample([marker])
    is_paused = True
    print_text_mid_screen('--- Paused ---')
    write_to_log(time.time(), 'Paused', marker)


# ------ Log ----------
def write_to_log(timestamp, text, marker):
  global log_lock, time_start
  time_passed = timestamp - time_start
  text_to_write = "[{:3.0f} : {:5.2f}]   {:20} - # {} \n".format(time_passed // 60, time_passed % 60, text, marker)
  with log_lock:
    with open(log_filename, 'a') as f:
      f.write(text_to_write)


# --------- Stimuli control -------
def show_stimuli(stimuli):
  global time_last_stimuli
  print(markers)
  print(stimuli)
  print(markers[stimuli])
  marker = markers[stimuli]
  if stimuli == '+':
    print_text_mid_screen(stimuli)
  elif stimuli == 'down':
    print_text_bottom_screen('x')
  elif stimuli == 'up':
    print_text_top_screen('x')
  elif stimuli == 'left':
    print_text_left_screen('x')
  else:  # stimuli == 'right'
    print_text_right_screen('x')

  time_last_stimuli = time.time()
  outlet.push_sample([marker])
  write_to_log(time_last_stimuli, stimuli, marker)


def find_next_stimuli(stimuli):
  global time_next_stimuli, next_stimuli
  if stimuli == '+':
    # tid random 1-2
    time_next_stimuli = time_last_stimuli + random.uniform(2, 3)
    # random ny marker
    next_stimuli = random.choices(["left", "right", "up", "down"],
                                  weights=[nbr_stim['tot'] - nbr_stim['left'], nbr_stim['tot'] - nbr_stim['right'],
                                           nbr_stim['tot'] - nbr_stim['up'], nbr_stim['tot'] - nbr_stim['down']])[0]

  else:
    nbr_stim[stimuli] += 1  # Count how many times each stimuli shown
    nbr_stim['tot'] += 1
    next_stimuli = "+"
    time_next_stimuli = time_last_stimuli + 1  # s


# -------- MAIN ---------------
if __name__ == '__main__':
  n_left = 0
  n_right = 0
  # callbackfunctions for keyboard response.
  keyboard.on_press_key("space", lambda _: space_is_pressed(), suppress=True)
  keyboard.on_press_key("q", lambda _: q_is_pressed(), suppress=True)
  keyboard.on_press_key("esc", lambda _: q_is_pressed(), suppress=True)

  # Steam that will send the markers
  stream_info = StreamInfo('Stimuli_program', 'Markers', 1, 0, 'int32', 'markers')
  outlet = StreamOutlet(stream_info)

  # Print welcome information
  welcome()
  print_text_mid_screen('--- Paused ---')
  time_start = time.time()
  dt_now = str(datetime.now())
  log_filename = "logs/left_right_trials_" + dt_now + ".txt"

  text = dt_now + " Trials started. \n"
  with log_lock:
    with open(log_filename, 'w') as f:
      f.write(text)

  # main loop that runs the program.
  while True:
    if is_closing == True:  # If q is pressed and trials closing
      print("\033[6B")
      print("\033[?25h")  # Make cursor visible again in terminal
      print("Left: {}, Right: {}, Up: {}, Down: {}".format(nbr_stim['left'], nbr_stim['right'], nbr_stim['up'],
                                                           nbr_stim['down']))
      exit(0)

    elif is_paused == True:  # if space bar is pressed and program is paused.
      time.sleep(0.2)

    else:  # The program is running. Just wait.
      time_to_wait = time_next_stimuli - time.time()
      if time_to_wait > 0.2:  # wait a bit more
        time.sleep(0.18)

      elif time_to_wait <= 0.03:  # If something should happen soon
        while time.time() < time_next_stimuli:  # Wait until right moment
          pass

        show_stimuli(next_stimuli)
        find_next_stimuli(next_stimuli)
