import matplotlib.pyplot as plt

from utilities.read_xdf_to_mne import *
from utilities import process_data
import numpy as np
from tensorflow.keras.models import load_model
from utilities.transform_data import transform_data

from mne_realtime import LSLClient, MockLSLStream

if __name__ == '__main__':

    # Load data.
    trial = "Erik_3"
    trials_resource_path = "./Trials"
    _, raw = process_data.get_data(trial_list=trial, trials_resource_path=trials_resource_path)
    raw = raw[0]
    events = mne.events_from_annotations(raw)

    # LSL host id and timeout time.
    host = 'Smarting'
    timeout_max_time = 5

    # Load trial information (sampling freq, no. channels etc).
    raw_info = np.load('raw_info.npy', allow_pickle='TRUE').item()

    # Load classifier model.
    model_path = './run_2020_09_09-15_33_35/model'
    model = load_model(model_path)

    # Let's observe it
    plt.ion()  # make plot interactive
    _, ax = plt.subplots(1)
    y_limits = 200
    colors = ['k', 'g', 'r', 'b', 'y']
    classes = ['None', 'left', 'right', 'up', 'down']

    with MockLSLStream(host, raw, 'eeg'):
        with LSLClient(info=raw_info, host=host, wait_max=timeout_max_time) as client:
            client_info = client.get_measurement_info()
            sfreq = client_info['sfreq']

            epoch_samples = 150
            epoch_time_length = epoch_samples * 1 / sfreq

            # t_max: seconds
            t_max = 50

            plt.cla()
            X = int(np.ceil(t_max * sfreq / epoch_samples).item())
            for ii in range(X):
                # Load the last n_samples samples from the stream.
                epoch = client.get_data_as_epoch(n_samples=epoch_samples)

                # Apply baseline correction from using time average in the interval (a,b).
                epoch = epoch.apply_baseline((None, 0.05))

                # Transform data for the classifier and classify epoch.
                data_trans = transform_data(epoch)
                pred_probdens = model.predict(data_trans)
                pred_class_indx = np.argmax(pred_probdens)
                pred_confidence = pred_probdens[0, pred_class_indx]

                data = epoch.get_data()[0]

                if pred_confidence >= 0.99:
                    move = pred_class_indx + 1  # Since 0 is the base color
                else:
                    move = 0

                iter_time = epoch_time_length * ii + epoch.times
                #iter_samples = np.round(iter_time * sfreq)

                plt.plot(iter_time, data.T * 1e6, colors[move])
                plt.ylim(-y_limits, y_limits)
                #plt.title('Classified as: {} (conf: {:.6f})'.format(classes[move], pred_confidence))
                plt.draw()
                #plt.pause(0.1)

    events = events[0]

    n_events = events.shape[0]
    for i in range(n_events):
        event_sample = events[i][0]
        event_time = event_sample * 1/sfreq
        event_id = events[i][2]

        if event_id == 1:
            color = 'm'
        elif event_id == 2:
            color = 'g'
        elif event_id == 3:
            color = 'r'
        elif event_id == 4:
            color = 'b'
        elif event_id == 5:
            color = 'y'
        else:
            print("Something strange happened")
            color = 'c'

        print(event_id)
        print(color)
        # 20 s
        if event_sample < 50*sfreq:
            plt.vlines(event_time, -y_limits, y_limits, color=color)

    ax = plt.gca()
    textstr = " Black: None \n Green: Left \n Red: Right \n Blue: Up \n Yellow: Down"
    props = dict(boxstyle='round', facecolor='white', alpha=0.7)
    ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=7,
            verticalalignment='top', bbox=props)

    plt.savefig("test2")

    # Let's terminate the mock LSL stream
    # stream.stop()
