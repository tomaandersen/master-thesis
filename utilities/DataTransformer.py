import numpy as np
from math import floor
from mne import events_from_annotations, create_info
from mne.io import RawArray

# TODO Important do something:
CHANNEL_COORDINATES = {'Fp1': [1, 0],
                       'Fp2': [3, 0],
                       'F3': [1, 1],
                       'F4': [3, 1],
                       'C3': [1, 2],
                       'C4': [3, 2],
                       'P3': [1, 4],
                       'P4': [3, 4],
                       'O1': [1, 5],
                       'O2': [3, 5],
                       'F7': [0, 1],
                       'F8': [4, 1],
                       'T7': [0, 2],
                       'T8': [4, 2],
                       'P7': [0, 4],
                       'P8': [4, 4],
                       'Fz': [2, 1],
                       'Cz': [2, 2],
                       'Pz': [2, 4],
                       'M1': [0, 3],
                       'M2': [4, 3],
                       'AFz': [2, 0],
                       'CPz': [2, 3],
                       'POz': [2, 5]}

CHANNEL_NAMES = list(CHANNEL_COORDINATES.keys())

# The EEG image size
CUBE_X, CUBE_Y = list(np.amax(np.array(list(CHANNEL_COORDINATES.values())), 0) + 1)


def merge_augmentation(augmented_data_cubes, epoch_labels):
    """
    Removes the augmentation dimension from the data, i.e.
    (n_epoch, n_win, n_samples, cube_dim1, cube_dim2) ->
    (n_epoch*n_win , n_samples, cube_dim1, cube_dim2)
    and duplicates the epoch labels so that each new epoch in the
    data cube gets it's corresponding label.

    The result is a data cube with it's original dimensions but
    with more epochs (i.e. it looks like we had more data to begin with!)

    Args:
        augmented_data_cubes: numpy array with dimensions
        (n_epoch, n_win, n_samples, cube_dim1, cube_dim2)

        epoch_labels: numpy array with dimension (n_epoch)

    Returns: Returns numpy array with dimension
    (n_epoch*n_win , n_samples, cube_dim1, cube_dim2)
    """
    n_epochs = augmented_data_cubes.shape[0]
    n_augmentations = augmented_data_cubes.shape[1]
    n_samples = augmented_data_cubes.shape[2]
    cube_x = augmented_data_cubes.shape[3]
    cube_y = augmented_data_cubes.shape[4]

    new_shape = (n_epochs * n_augmentations, n_samples, cube_x, cube_y)

    return augmented_data_cubes.copy().reshape(new_shape)


def sliding_window_augmentation(data_cubes, win_size, stride=1, k_start=0, k_stop=None):
    """
    Sliding window. Will return floor(((k_stop - k_start) - win_size)/stride)+1
    number of windows. The sliding occur in the sample dimension of the data_cube

    Args:
        data_cubes: numpy array with dimensions
        (n_epochs, n_samples, cube_dim1, cube_dim2).

        win_size: window size to use. Must be positive integer < n_samples.
        stride: integer specifying the sample shift between windows.

        k_start: first sample to include. The first sample of the first window
        will be this sample.

        k_stop: last sample to include. Note that this sample is necessary not included
        in the last window if stride>1.

    Returns: Returns a list of data_cubes lists, i.e. numpy array
    with dimension [n_epoch, n_win, n_samples, cube_dim1, cube_dim2]
    """
    n_epochs = data_cubes.shape[0]
    n_samples = data_cubes.shape[1]
    cube_x = data_cubes.shape[2]
    cube_y = data_cubes.shape[3]

    if k_stop is None:
        k_stop = n_samples

    # Number of windows formula:
    n_samples_to_use = k_stop - k_start
    n_win = floor((n_samples_to_use - win_size) / stride) + 1

    # dim: [n_epoch, n_win, n_samples, cube_dim1, cube_dim2]
    data_cubes_augmented = np.zeros((n_epochs, n_win, win_size, cube_x, cube_y))

    for i in range(0, n_win):
        # Indices to use in current window:
        start_indx = k_start + i * stride
        stop_indx = start_indx + win_size
        win_indx = np.arange(start_indx, stop_indx)

        # Get window from data_cube sample dimension.
        window = data_cubes[:, win_indx, :, :]

        # Insert the i:th element into the np matrix.
        data_cubes_augmented[:, i, :, :, :] = window

    return data_cubes_augmented


def noise_addition_augmentation(data_cubes, noise_power):
    """
    https://www.sciencedirect.com/science/article/pii/S0165027020303083
    """
    raise Exception("Method not yet implemented")


def annotations2stim(raw):
    """
    Adds stim-channels to the raw-object based on it's annotations.
    Args:
        raw: raw object to use
    Returns: raw-object with added stim-channels.
    """
    events, event_names = events_from_annotations(raw)

    # Extract event samples and event ids.
    event_samples = events[:, 0]
    event_ids = events[:, 2]
    no_event_types = len(np.unique(event_ids))

    # Create X numbers of stim channels
    # Events are in the range [1...no_events]
    stim = np.zeros((no_event_types, len(raw.times)))
    stim_names = []
    for event_index in range(1, no_event_types + 1):
        # Get samples where specific event (event_index) happen
        indx = np.where(event_ids == event_index)
        t = event_samples[indx]

        stim[event_index - 1, t] = 30E-6  # Small but non-zero => Easier to see in plot with eeg signals.
        stim_names.append('STI{}'.format(event_index - 1))

    # Create info and then add the new stim-channel to the raw-object.
    info = create_info(stim_names, raw.info['sfreq'], ['eeg'] * no_event_types)
    raw.add_channels([RawArray(stim, info)], force_update_info=True)

    return raw


def get_stim_events(epoch, stim_prefix='STI'):
    """
    Returns events that is present in the epoch based on information in the stim-channels (must be named STI<int>).
    Args:
        stim_prefix: Prefix of channel names that should be interpreted as stim-channels.
        epoch: Epoch to use
    Returns: Returns list of events
    """

    ch_data = epoch.get_data()
    stim_ch_index = get_stim_channel_index(epoch, stim_prefix)
    stim_ch_data = ch_data[0, stim_ch_index, :]

    # Check if the channel data contains any non-zero values. If so then event at that channel have occurred.
    event_index = np.where(np.any(stim_ch_data != 0, axis=1))

    return event_index[0]  # ([event_indices])[0] = [... event_indices ...]


def get_eeg_channel_index(epoch, stim_prefix='STI'):
    n_channels = epoch.n_channels
    index = np.arange(0, n_channels)
    stim_ch_index = get_stim_channel_index(epoch)
    eeg_ch_index = np.delete(index, stim_ch_index)
    return eeg_ch_index

def drop_stim_channels(epoch, stim_prefix='STI'):
    """
    Removes any channels starting with the stim prefix.
    Args:
        epoch: Epoch
        stim_prefix: Stim prefix (by default "STI")

    Returns: Returns epoch without stim-channels
    """
    stim_ch_index = get_stim_channel_index(epoch, stim_prefix)
    if len(stim_ch_index) > 0:
        stim_names = np.array(epoch.ch_names)[stim_ch_index]
        epoch.drop_channels(stim_names.tolist())
    return epoch

def get_stim_channel_index(epoch, stim_prefix='STI'):
    ch_indices = []
    ch_index = 0
    for ch_name in epoch.ch_names:
        if stim_prefix in ch_name:
            ch_indices.append(ch_index)
        ch_index += 1
    return ch_indices


def epochs2cubes(epochs):
    """ TODO ADD MORE
    Converts MNE Epochs objects into numpy matrices.
    Args:
        epochs: MNE Epochs object to be transformed
    Returns:
        Returns an numpy matrix with the dimensions
        (n_epochs, n_samples (time), cube_dim1 (spatial 1), cube_dim2 (spatial 2)).
        cube_dim1 and cube_dim2 are the dimensions of the eeg image.
    """
    # shape: [epochs, channels, time]
    epochs_data = epochs.get_data()

    n_epochs = epochs_data.shape[0]
    n_samples = epochs_data.shape[2]

    data_cubes = np.zeros((n_epochs, n_samples, CUBE_X, CUBE_Y))

    # Rearrange the channels according to the specified coordinates.
    for channel in CHANNEL_NAMES:
        channel_index = CHANNEL_NAMES.index(channel)
        channel_data = epochs_data[:, channel_index, :]
        channel_coords = CHANNEL_COORDINATES.get(channel)
        data_cubes[:, :, channel_coords[0], channel_coords[1]] = channel_data

    return data_cubes


if __name__ == '__main__':
    import utilities.process_data as prd
    import utilities.transform_data as tfd

    trial_list = 'Erik_1'
    trials_resource_path = '../Trials'

    _, raw_list = prd.get_data(trial_list, trials_resource_path)
    raw = raw_list[0]
    raw = annotations2stim(raw.copy())
    epochs = prd.get_epochs_from_raw(raw)

    print(epochs.ch_names)
    epoch = drop_stim_channels(epochs)
    print(epochs.ch_names)
