#!python3

'''
This file uses the final processing stuff I found was good. Use analyze data to get nice plots of stuff. 
'''

import mne
import numpy as np
from utilities.read_xdf_to_mne import *
from glob import glob
import logging
import os
from autoreject import get_rejection_threshold, compute_thresholds  # To automatically get rejection treshold
import matplotlib as mpl
import matplotlib.pyplot as plt

# This is never used I believe
markers = {'+': 0000,
           'left': 1001,
           'right': 2001,
           'up': 3001,
           'down': 4001,
           'eyeblink': 7000,
           'nothing': 7001,
           'start_pause': 8001,
           'end_pause': 8002,
           'end_trial': 9999}


def _concat_lists(epochs_list, raw_list):
    """
    Concatenates list of epochs and raw objects into one epochs and raw object.
    """
    # ----- Combining raws -------
    if len(raw_list) > 1:
        raw = mne.concatenate_raws(raw_list)
    else:
        raw = raw_list[0]

    # ---- Combining epochs ------
    if len(epochs_list) > 1:
        epochs = mne.concatenate_epochs(epochs_list)
    else:
        epochs = epochs_list[0]

    return epochs, raw

def _check_(event_id, event_dict_interesting):
    event_indices = list(event_id.values())
    interesting_event_indices = list(event_dict_interesting.values())

    # Check if all interesting events are present in trial events.
    if not set(interesting_event_indices) <= set(event_indices):
        raise Exception("Specified interesting events are not present in the chosen trial.")

def get_data(trial_list, trials_resource_path="./Trials"):
    """ Get data based on trial name
    Args:
        trial_list: List of folder names to trials.
        trials_resource_path: Path to resource directory
        where all the trials folders are places.
    Returns:
        Returns path to xdf files and raw data.
    """
    logging.info("Loading data from resource folder: {}".format(os.path.abspath(trials_resource_path)))

    # Make sure that trial_list is a list even if it only has one entry
    if not isinstance(trial_list, list):
        trial_list = [trial_list]

    # Get list of paths to trials folders
    trials_folder_list = [(trials_resource_path + "/{}").format(e) for e in trial_list]

    # Get list of paths to all data files and their corresponding xdf data.
    fnames = []
    raw_list = []
    for folder in trials_folder_list:
        data_files = glob(folder + "/*.xdf")

        # Multiple data files are found in trial folder. Not expected, notify user.
        if len(data_files) > 1:
            logging.warning("Found multiple data files while loading data in {}. Only one will be used.".format(folder))

        if len(data_files) > 0:
            data_file = data_files[0]
            fnames.append(data_file)
            raw_list.append(read_raw_xdf(data_file))

    if len(raw_list) == 0:
        logging.warning("No trials found while loading data from resource folder {}".format(os.path.abspath(trials_resource_path)))

    return fnames, raw_list


# TODO change the event_dict_interesting. There must be a prettier way to handle this.
def get_epochs_from_raw(raw, event_dict_interesting = {'left': 2, 'right': 3, 'up': 4, 'down': 5}):
    """
    Get epochs from raw object. Also does a quick check that the trial procedure is followed.
    Args:
        raw: A raw object.

    Returns:
        epochs: returns an epochs object.
    """

    events, events_id = mne.events_from_annotations(raw)

    # Remove que if following is not back to +
    for i in range(len(events)):
        event = events[i]
        event_int = event[2]  # What "marker" for that event in mne language.

        # if left, right, up, down or start_pause.
        if event_int in [2, 3, 4, 5]:
            next_event = events[i + 1]
            # Assume the trial is done correctly by quitting with Esc/q before stream is ended.

            if next_event[2] != 1:
                event[2] = 99  # Remove instance

    # TODO Take a look at the remove instance thing above. How does that affect the epochs below...?

    # Create epochs with rejection
    epochs = mne.Epochs(raw, events, event_id=event_dict_interesting,
                        tmin=-0.3, tmax=0.9, preload=True, baseline=(-0.3, -0.1))
    return epochs


def process_data(trial_list=["Erik_1", "Erik_2", "Erik_3"]):
    """ Loads and processes data from a trial. From this file: the trials as xdf files should be placed ./Trials_everything/trial_name/
    In the same folder as this should read_xdf_to_mne be placed.

    Input:
        Trial name of the trial to process.
    Returns:
        epochs of data divided in left/right and raw data as: [epochs, raw]
    """
    # Make sure that trial_list is a list even if it only has one entry
    if not isinstance(trial_list, list):
        trial_list = [trial_list]

    # Load raw data into list of raw objects.
    _, raw_list = get_data(trial_list)

    # Any preprocessing of raw objects should be done here:
    for raw in raw_list:
        raw.filter(l_freq=2, h_freq=40)

    # Create epochs. For each raw create epochs.
    epochs_list = []
    event_dict_interesting = {'left': 2, 'right': 3, 'up': 4, 'down': 5}
    for raw in raw_list:
        epochs = get_epochs_from_raw(raw, event_dict_interesting)
        epochs_list.append(epochs)

    # From the list of epochs and raws objects create one big epochs and raw object.
    epochs, raw = _concat_lists(epochs_list, raw_list)

    return epochs, raw


#
# Experimental stuff below.
# If epochs with nothing is included, is the classification improved?
#
#
# =================================================================================
# ========= ADD NOTHING ==========
# =================================================================================

def _add_annotations_of_nothing(raw, trial_name):
    if trial_name == "Erik_1":
        onset_list = [7.0, 21.16, 28.6, 31.7, 35.1, 41.83, 49.26, 56.66, 60.5, 64.35, 68.14, 89.04, 106.99, 128.72,
                      158.87, 169.61, 187.12, 211.68, 218.84, 257.56]

    elif trial_name == "Erik_2":
        onset_list = [8.03, 15.76, 19.63, 43.98, 50.54, 57.26, 67.42, 74.49, 91.39, 109.29, 136.15, 143.29, 153.29,
                      190.20, 207.38, 231.61, 256.22, 288.21, 308.9, 316.18]

    elif trial_name == "Erik_3":
        onset_list = [10.75, 14.19, 28.13, 31.44, 48.45, 66.46, 86.95, 103.51, 138.36, 146.07, 149.44, 176.15, 191.05,
                      211.64, 232.76, 247.94, 265.54, 286.40, 297.97, 301.65]

    for onset in onset_list:
        raw.annotations.append(onset=onset, duration=0.0, description='7001')

    return raw


def _add_annotations_of_eyes(raw, trial_name):
    if trial_name == "Erik_1":
        onset_list = [1.09, 4.97, 10.84, 14.08, 17.22, 24.70, 25.78, 27.60, 38.16, 45.08, 52.46, 172.62, 232.18, 235.86,
                      239.40, 247.42, 250.64, 269.56]
    elif trial_name == "Erik_2":
        onset_list = [5.54, 7.31, 11.14, 14.57, 22.60, 29.38, 33.17, 46.85, 70.69, 84.25, 88.86, 119.08, 139.21, 225.24,
                      228.43, 245.18, 259.86, 267.44, 270.60, 278.62, 325.37]
    elif trial_name == "Erik_3":
        onset_list = [5.92, 7.14, 24.32, 53.01, 97.22, 110.97, 124.65, 128.24, 134.63, 153.89, 157.66, 161.58, 164.96,
                      173.00, 205.18, 215.51, 222.32, 236.99, 283.50, 294.99, 312.32]

    for onset in onset_list:
        raw.annotations.append(onset=onset - 0.33, duration=0.0,
                               description='7000')  # Set marker 0.33 s before the eyblink to be consistient with other markings.

    return raw


def process_data_add_nothing_and_eyeblinks(trial_list=["Erik_1", "Erik_2", "Erik_3"], trials_resource_path="./Trials"):
    '''
    Loads and processes data from a trial. From this file: the trials as xdf files should be placed ./Trials_everything/trial_name/
    In the same folder as this should read_xdf_to_mne be placed.

    Input:
        Trial name of the trial to process.
    Returns:
        epochs of data divided in left/right and raw data as: [epochs, raw]
    '''

    # Make sure that trial_list is a list even if it only has one entry
    if not isinstance(trial_list, list):
        trial_list = [trial_list]

    # Load raw data into list of raw objects.
    _, raw_list = get_data(trial_list, trials_resource_path)

    # Any preprocessing of raw objects should be done here:
    for i in range(len(raw_list)):
        raw = raw_list[i]

        # Apply standard bandpass filter.
        raw.filter(l_freq=2, h_freq=40)

        # Add annotations of nothing and eyeblinks.
        raw = _add_annotations_of_nothing(raw, trial_list[i])
        raw = _add_annotations_of_eyes(raw, trial_list[i])

        # Insert back the raw object into the list of raw objects.
        raw_list[i] = raw.copy()

    # Create epochs. For each raw create epochs.
    # TODO: place this (event_dict_interesting) somewhere else. Make it pretty.
    event_dict_interesting = {'left': 2, 'right': 3, 'up': 4, 'down': 5, 'eyeblink': 6, 'nothing': 7}
    epochs_list = []
    for raw in raw_list:
        epochs = get_epochs_from_raw(raw, event_dict_interesting)
        epochs_list.append(epochs)

    # From the list of epochs and raws objects create one big epochs and raw object.
    epochs, raw = _concat_lists(epochs_list, raw_list)

    return epochs, raw
