# !python3

'''
ML on EEG data. 
'''
import numpy as np
import matplotlib.pyplot as plt

ch_coord = {'Fp1': [1, 0],
            'Fp2': [3, 0],
            'F3': [1, 1],
            'F4': [3, 1],
            'C3': [1, 2],
            'C4': [3, 2],
            'P3': [1, 4],
            'P4': [3, 4],
            'O1': [1, 5],
            'O2': [3, 5],
            'F7': [0, 1],
            'F8': [4, 1],
            'T7': [0, 2],
            'T8': [4, 2],
            'P7': [0, 4],
            'P8': [4, 4],
            'Fz': [2, 1],
            'Cz': [2, 2],
            'Pz': [2, 4],
            'M1': [0, 3],
            'M2': [4, 3],
            'AFz': [2, 0],
            'CPz': [2, 3],
            'POz': [2, 5]}

ch_list = list(ch_coord.keys())

def transform_all_types_of_epochs(epochs, epochs_labels, start_time=-np.inf, end_time=np.inf, win_size=None,
                                  time_shift=None):
    data = []
    labels = []
    for i in range(len(epochs_labels)):  # Go through all labels and find their data
        epochs_label = epochs_labels[i]
        data_here = transform_data(epochs[epochs_label], start_time=start_time, end_time=end_time, win_size=win_size,
                                   time_shift=time_shift)
        labels_here = np.ones(data_here.shape[0]) * i
        data.extend(data_here)
        labels.extend(labels_here)

    c = list(zip(data, labels))
    np.random.shuffle(c)
    np.random.shuffle(c)
    data, labels = zip(*c)

    labels = np.array(labels)
    data = np.array(data)

    return data, labels


def transform_data(epochs, start_time=-np.inf, end_time=np.inf, win_size=None, time_shift=None):
    '''
	Function for transforming data to 3d cube, also pick a certain window if specified

	Input: 
		start_time 	- First time to include.
		end_time	- last time to include.

	Returns:
		X - [batches,timesteps,(5x6)image of eeg-channels]
	'''

    times = epochs.times
    ch_list = epochs.ch_names

    Fs = 500

    # epochs.get_data().shape => (16, 24, 601)    (epoch number x eeg channels x time)
    # (0.9+0.3) * 500 = 600, I don't know where the +1 comes from.
    data_array = epochs.get_data()
    n_batches = data_array.shape[0]
    if win_size is not None:
        win_steps = int(win_size * Fs)

    if win_size != None and time_shift != None:
        last_time_start = end_time - win_size

        indices_to_use_list = []
        for s_time in np.arange(start_time, last_time_start, time_shift):
            ind = np.full(len(times), False)
            first_ind = times.searchsorted(s_time)
            ind[first_ind:first_ind + win_steps] = np.full(win_steps, True)
            indices_to_use_list.append(ind)
    else:
        # If no window is selected we use the time that is in [start_time...end_time]
        indices_to_use_list = [np.logical_and(times >= start_time, times <= end_time)]

    # indices_to_use.shape => (6, 601)
    data = []
    for indices_to_use in indices_to_use_list:

        for batch in range(n_batches):  # Go through all batches.
            orig_data = data_array[batch]

            # transformed_data.shape = (150, 5, 6)
            # TODO: can we just use win_steps instead of the sum below..?
            # TODO: No, as win_steps might be None
            transformed_data = np.zeros((np.sum(indices_to_use), 5, 6))
            # sum(indices_to_use) is corresponding to nbr of time points to use.

            # orig_data.shape => (24, 601)
            # For each channel in batch...
            for ch in ch_list:
                ch_data = orig_data[ch_list.index(ch), indices_to_use]

                this_channel_coords = ch_coord[ch]
                transformed_data[:, this_channel_coords[0], this_channel_coords[1]] = ch_data
                # e.g. transformed_data[:,1,0] for Fp1

            data.append(transformed_data)

    data = np.array(data)
    data = data[:, :, :, :, np.newaxis]  # insert one extra axis because the data should have one channel in 3Dconv

    return data


def rearange_array(data_in):
    '''
	Input one set of data to rearrange, for real-time rearange of data. 
	'''

    data = []

    transformed_data = np.zeros(
        (data_in.shape[1], 5, 6))  # sum(indices_to_use) is corresponding to nbr of time points to use.

    for ch in ch_list:  # Go through all channels and place them right
        ch_data = data_in[ch_list.index(ch), :]
        transformed_data[:, ch_coord[ch][0], ch_coord[ch][1]] = ch_data  # e.g. transformed_data[:,1,0] for Fp1

    data.append(transformed_data)
    data = np.array(data)
    data = data[:, :, :, :, np.newaxis]  # return data in a format that the neural network can use directly.
    return data


def plot_one_batch(data, batch_nbr=0):
    plt.ion()
    data_to_use = data[batch_nbr]
    # Start by plotting first time step
    plt.matshow(data_to_use[0, :, :, 0] * 1e6, 0, vmin=-80, vmax=80)
    plt.colorbar()
    plt.pause(0.001)
    for i in range(1, 100):  # data_to_use.shape[0]):
        plt.matshow(data_to_use[i, :, :, 0].T * 1e6, 0, vmin=-80, vmax=80)
        plt.pause(0.001)
        plt.draw()



if __name__ == '__main__':

    from utilities.process_data import get_data
    # Load data.
    trial = "Erik_3"
    trials_resource_path = "../Trials"
    _, raw = get_data(trial_list=trial, trials_resource_path=trials_resource_path)
    raw = raw[0]



