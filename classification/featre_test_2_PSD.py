import utilities.process_data as prd
import numpy as np
import matplotlib.pyplot as plt
import scipy
from math import floor
import mne
import time


def psd_power_ratio(data_cube, f_middle_indx):
    fs = 500
    n_channels = data_cube.shape[0]

    lower_psd_sum_list = []
    upper_psd_sum_list = []

    psd_ratio_list = np.zeros(n_channels)
    for ch_index in range(n_channels):
        ch_time_series = data_cube[ch_index, :]
        _, psd = scipy.signal.welch(ch_time_series, fs=fs)

        lower_psd_sum = np.sum(psd[f_middle_indx:])
        upper_psd_sum = np.sum(psd[:f_middle_indx])
        lower_psd_sum_list.append(lower_psd_sum)
        upper_psd_sum_list.append(upper_psd_sum)
        psd_ratio_list[ch_index] = lower_psd_sum/upper_psd_sum

    return upper_psd_sum_list, lower_psd_sum_list

if __name__ == '__main__':
    trial = ["Erik_3", "Erik_2", "Erik_1"]
    trials_resource_path = "../Trials"

    epochs, raw = prd.process_data_add_nothing_and_eyeblinks(trial_list=trial, trials_resource_path=trials_resource_path)
    interesting_channels = np.arange(len(epochs.ch_names))
    event_types = ['left', 'right', 'up', 'down', 'eyeblink', 'nothing']
    colors = ['r', 'b', 'g', 'c', 'm', 'k']

    fs = 500
    f_middle_index = 5

    n_channels = len(epochs.ch_names)

    #one_data_cube = epochs.get_data()[0]
    #psdr = psd_power_ratio(one_data_cube, f_middle_index)
    #print(psdr)
    #quit()

    fig = plt.figure()
    ax = fig.add_subplot(111)

    event_indx = 0
    for event in event_types:

        c = colors[event_indx]
        u_list_mean = []
        l_list_mean = []

        start = time.time()
        for data_cube in epochs[event]:
            u_list, l_list = psd_power_ratio(data_cube, f_middle_index)
            u_list_mean.append(np.mean(u_list))
            l_list_mean.append(np.mean(l_list))
        stop = time.time()
        print("PSD power ratio calculated for 24 channels: {}".format(stop-start))

        ax.plot(u_list_mean, l_list_mean, '+{}'.format(c))
        event_indx += 1

    ax.set_xlabel("High frequency power")
    ax.set_ylabel("Low frequency power")

    ax.legend(event_types)
    plt.draw()
    plt.pause(100)