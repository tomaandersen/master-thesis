import utilities.process_data as prd
import numpy as np
import matplotlib.pyplot as plt
import scipy
import mne
import time

if __name__ == '__main__':
    trial = ["Erik_3", "Erik_2", "Erik_1"]
    trials_resource_path = "../Trials"

    epochs, raw = prd.process_data_add_nothing_and_eyeblinks(trial_list=trial, trials_resource_path=trials_resource_path)
    interesting_channels = np.arange(len(epochs.ch_names))
    event_types = ['left', 'right', 'up', 'down', 'eyeblink', 'nothing']
    colors = ['r', 'b', 'g', 'c', 'm', 'k']

    fig = plt.figure()
    fig2 = plt.figure()

    ax = fig.add_subplot(111)
    ax2 = fig2.add_subplot(111)


    #ica = mne.preprocessing.ICA(n_components=15, random_state=97)
    #ica.fit(raw)

    data = epochs.get_data()
    n_classes = len(np.unique(epochs.events[:, 2]))
    n_channels = len(epochs.ch_names)

    for ch in range(n_channels):

        channel_data = data[:, ch, :]
        f1 = np.mean(channel_data, axis=1)
        f2 = np.var(channel_data, axis=1)

        for cls in range(n_classes):
            epoch_cls_indx =
            ax2.plot(f1, f2)

        ax2.draw()
        ax2.set_title(epochs.ch_names[ch])
        ax2.set_xlabel("f1")
        ax.set_ylabel("f2")

        plt.waitforbuttonpress()


    quit()
    event_name_index = -1
    # For each event type...
    for event_name in event_types:
        event_name_index += 1

        ep = epochs[event_name]
        data_cubes = ep.get_data()
        n_epochs = data_cubes.shape[0]
        n_channels = data_cubes.shape[1]
        n_samples = data_cubes.shape[2]
        n_features = 2

        features = np.zeros((n_epochs, len(interesting_channels), n_features))

        # For each epoch...
        for e in range(n_epochs):
            data_cube = data_cubes[e]  # One epoch data

            # Drop uninteresting channels
            data_cube = data_cube[interesting_channels, :]

            features[e, :, 0] = scipy.stats.kurtosis(data_cube, axis=1)
            features[e, :, 1] = np.var(data_cube, axis=1)
            # features[e, :, 2] = np.mean(data_cube, axis=1)
            # shape: (n_epochs, n_channels, n_features)

        features2 = np.mean(features, axis=1)
        x = features2[:, 0]
        y = features2[:, 1]
        c = colors[event_name_index]
        ax.plot(x, y, '+{}'.format(c))
        plt.pause(0.001)

    ax.set_xlabel("Kurtosis")
    ax.set_ylabel("Variance")

    plt.draw()
    plt.pause(1000)




