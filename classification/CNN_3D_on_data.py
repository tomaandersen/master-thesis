# !python3


'''
CNN 3D on data. 
'''

import matplotlib.pyplot as plt
from utilities.transform_data import transform_all_types_of_epochs
from utilities.process_data import process_data_add_nothing_and_eyeblinks
from tensorflow import keras
from sklearn.model_selection import train_test_split
import os

# ================================
# ==== Parameters to change ======
# ================================
trials = [["Erik_1", "Erik_2"], "Erik_3"]  # The first one is the one used for training the network.
epochs_labels = ['left', 'right', 'up', 'down', 'eyeblink',
                 'nothing']  # Note this, this is the order of the labels in the model!!!
start_time = 0.15
end_time = 0.55
win_size = 0.3
time_shift = 0.02
# ================================


# ================================
# ============ Notes =============
# ================================

'''
This is the first script with nothing happens addenes and eyeblink classes.

'''


# ================================


def get_run_logdir():
    global run_id
    import time
    run_id = time.strftime("run_%Y_%m_%d-%H_%M_%S")
    return os.path.join(root_logdir, run_id)


# ----- Create run directory to store stuff in ------
run_id = None

root_logdir = os.path.join(os.curdir, "../runs")  # Should be standing in Analyze_data
run_logdir = get_run_logdir()
os.mkdir(run_logdir)

# ---- Loading, preprocessing and slicing data. ----
trial = trials[0]
epochs, _ = process_data_add_nothing_and_eyeblinks(trial)


data, labels = transform_all_types_of_epochs(epochs, epochs_labels, start_time=start_time, end_time=end_time,
                                             win_size=win_size, time_shift=time_shift)

X_x, X_test, y_y, y_test = train_test_split(data, labels, test_size=0.15)
X_train, X_val, y_train, y_val = train_test_split(X_x, y_y, test_size=0.17)

# Det ovan är inte helt rätt, testar på data som jag tränat på. Det bör göras något i stil med följande i stället. 
# Jag har inte testat denna koden för detta experimentet men för ett annat så den bör fungera ungefär rätt. Det som kan vara
# fel är ep_la = epochs.events[:,2]-1, kan vara att det ska vara -0 eller -2 i stället, jag har inte helt tänkt klart på det just nu. 
# # ---- Loading, preprocessing and slicing data. ---- 
# epochs,_ = process_data(trial)

# ep_la = epochs.events[:,2]-1 # This is the labels for the epochs. 

# # Split the epochs to different sets, get the sets indices. 
# ind_train_val, ind_test = train_test_split(range(len(ep_la)), stratify=ep_la, test_size=0.15)
# ind_train, ind_val = train_test_split(ind_train_val, stratify=ep_la[ind_train_val], test_size=0.17)


# X_train, y_train = transform_all_types_of_epochs(epochs[ind_train],epochs_labels, start_time=start_time, end_time=end_time, win_size=win_size, time_shift=time_shift)
# X_val, y_val = transform_all_types_of_epochs(epochs[ind_val],epochs_labels, start_time=start_time, end_time=end_time, win_size=win_size, time_shift=time_shift)
# X_test, y_test = transform_all_types_of_epochs(epochs[ind_test],epochs_labels, start_time=start_time, end_time=end_time, win_size=win_size, time_shift=time_shift)

steps = X_train.shape[1]

# --------- Model creation --------
# How big the model is depends a lot on how big the data in is!!!
model = keras.Sequential([
    keras.layers.AveragePooling3D(pool_size=(10, 1, 1), strides=(2, 1, 1), padding='valid',
                                  input_shape=[steps, 5, 6, 1]),  #
    keras.layers.SpatialDropout3D(0.3),
    keras.layers.Conv3D(filters=10, kernel_size=(10, 2, 2), strides=(1, 1, 1), padding='valid', activation='elu'),  #
    keras.layers.Conv3D(filters=10, kernel_size=(10, 2, 2), strides=(1, 1, 1), padding='valid', activation='elu'),
    keras.layers.AveragePooling3D(pool_size=(4, 1, 1), strides=(2, 1, 1), padding='valid'),
    keras.layers.SpatialDropout3D(0.2),
    keras.layers.Conv3D(filters=20, kernel_size=(4, 2, 2), strides=(1, 1, 1), padding='same', activation='elu'),
    keras.layers.Conv3D(filters=20, kernel_size=(4, 2, 2), strides=(1, 1, 1), padding='same', activation='elu'),
    keras.layers.AveragePooling3D(pool_size=(4, 1, 1), strides=(2, 1, 1), padding='valid'),
    # # keras.layers.SpatialDropout3D(0.2),
    # keras.layers.Conv3D(filters=40, kernel_size=(4,1,2),strides=(1,1,1),padding='valid',activation='elu'),
    # keras.layers.Conv3D(filters=40, kernel_size=(4,1,1),strides=(1,1,1),padding='valid',activation='elu'),
    # keras.layers.AveragePooling3D(pool_size=(2,1,1),strides=(2,1,1),padding='valid'),
    keras.layers.Flatten(),
    keras.layers.Dense(units=30, activation='elu'),
    keras.layers.Dropout(0.5),
    keras.layers.Dense(units=10, activation='elu'),
    keras.layers.Dropout(0.5),
    keras.layers.Dense(units=6, activation='softmax'),
])

model.summary()
optimizer = keras.optimizers.Adam(learning_rate=0.0005)
model.compile(optimizer=optimizer, metrics=['accuracy'], loss="sparse_categorical_crossentropy")

print(" ----  Train model -----")

# Create checkpoint path in "runs" folder
checkpoint_path = '{}/checkpoints/'.format(run_logdir)
checkpoint_filepath = '{}model'.format(checkpoint_path) + '-{epoch:02d}-{val_loss:.2f}.hdf5'
os.makedirs(checkpoint_path)

callbacks = [  # keras.callbacks.EarlyStopping(patience=5000, monitor="val_accuracy",restore_best_weights=False),
    keras.callbacks.TensorBoard(run_logdir, histogram_freq=1),
    keras.callbacks.ModelCheckpoint(filepath=checkpoint_filepath)]

# Train model
epoch_count = 10
hist = model.fit(X_train, y_train, epochs=epoch_count, validation_data=(X_val, y_val), callbacks=callbacks)

if 1:
    plt.figure()
    plt.plot(hist.history['loss'], label='MAE (training data)')
    plt.plot(hist.history['val_loss'], label='MAE (validation data)')
    plt.title('Loss for Left/right')
    plt.ylabel('Loss value')
    plt.xlabel('No. epoch')
    plt.legend(loc="upper left")

    print(hist.history)
    # Plot history:
    plt.figure()
    plt.plot(hist.history['accuracy'], label='accuracy (training data)')
    plt.plot(hist.history['val_accuracy'], label='accuracy (validation data)')
    plt.title('Accuracy for left/right')
    plt.ylabel('Accuracy value')
    plt.xlabel('No. epoch')
    plt.legend(loc="upper left")

print(" ----  Evaluate model -----")
res = model.evaluate(X_test, y_test)
with open("{}/test_score.txt".format(run_logdir), "a") as file:
    file.write("Test on same session ({}) - loss: {}, accuracy: {}\n".format(trial, res[0], res[1]))
print("Test on same data ({}) - loss: {}, accuracy: {}".format(trial, res[0], res[1]))

## ------- Load and save model ----------------
if 1:
    model.save('{}/model'.format(run_logdir))

if 0:
    model = load_model('{}/final_model'.format(run_logdir))

## ------ Test model other data ------

for i in range(1, len(trials)):
    trial = trials[i]
    epochs, _ = process_data_add_nothing_and_eyeblinks(trial)
    data, labels = transform_all_types_of_epochs(epochs, epochs_labels, start_time=start_time, end_time=end_time,
                                                 win_size=win_size, time_shift=time_shift)
    res = model.evaluate(data, labels)
    with open("{}/test_score.txt".format(run_logdir), "a") as file:
        file.write("Test on other session ({}) - loss: {}, accuracy: {}\n".format(trial, res[0], res[1]))
    print("Test on other data ({}) - loss: {}, accuracy: {}".format(trial, res[0], res[1]))

plt.show()
