import utilities.process_data as prd
import utilities.DataTransformer as dtt
import numpy as np
from mne_realtime import MockLSLStream, LSLClient
import matplotlib.pyplot as plt
from mne import Epochs


if __name__ == '__main__':
    trial = "Erik_3"
    trials_resource_path = "./Trials"
    _, raw = prd.get_data(trial_list=trial, trials_resource_path=trials_resource_path)
    raw = raw[0]

    raw = dtt.annotations2stim(raw.copy())

    raw_info = raw.info
    host = 'Smarting'

    current_sample = 0
    epoch_samples = 150
    server = MockLSLStream(host, raw, 'eeg')
    client = LSLClient(info=raw_info, host=host, wait_max=5, verbose=False)

    server.start()
    client.start()

    _, ax = plt.subplots(1)
    plt.plot()

    print("Starting LSL Client loop")

    epoch_index = 0
    while True:
        epoch = client.get_data_as_epoch(n_samples=epoch_samples)
        epoch.apply_baseline((0, 0.05), verbose=False)

        events = dtt.get_stim_events(epoch)
        if len(events) > 0:
            print(events)
        else:
            pass

        dtt.drop_stim_channels(epoch)
        data = epoch.get_data()[0]

        plt.plot(epoch_index * epoch_samples + np.arange(0, epoch_samples), data.T * 1e6)
        plt.draw()
        plt.pause(0.0001)

        epoch_index += 1

    print("Stopping LSL Client loop")
    client.stop()
    server.stop()


