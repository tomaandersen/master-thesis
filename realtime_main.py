from utilities import process_data, transform_data
import numpy as np
from mne_realtime import MockLSLStream
from realtime_client import RealTimeClient
from game.game_client import GameClient
import logging
from tensorflow.keras.models import load_model
from mne import set_log_level
import utilities.DataTransformer as dt
import matplotlib.pyplot as plt

if __name__ == '__main__':

    def log(msg):
        print("[Main] {}".format(msg))


    set_log_level('WARNING')

    # ---- Configurations
    lsl_host = 'Smarting'
    ipc_host = ('localhost', 16000)
    ipc_authkey = b'salamimackaegott'
    game_size = [10, 10]
    epoch_samples = 150
    raw_info_fname = 'raw_info.npy'
    mock_server = True
    connect_to_game = False

    # ---- Classifier
    # Load classifier model.
    model_path = './run_2020_09_09-15_33_35/model'
    classifier = load_model(model_path)

    # ---- Game and LSL clients
    # Is overrides if mock server is on. Change this, it's ugly
    raw_info = np.load(raw_info_fname, allow_pickle='TRUE').item()

    # ---- Start mock LSL server
    if mock_server:
        # Load data.
        trial = "Erik_3"
        trials_resource_path = "./Trials"
        _, raw = process_data.get_data(trial_list=trial, trials_resource_path=trials_resource_path)
        raw = raw[0]

        # Add stim channels
        raw = dt.annotations2stim(raw.copy())
        raw_info = raw.info

        # Using modified MockLSLStream
        server = MockLSLStream(host=lsl_host, raw=raw, ch_type='eeg')
        server.start()


    game_client = GameClient(game_size=game_size, address=ipc_host, authkey=ipc_authkey)
    lsl_client = RealTimeClient(info=raw_info, host=lsl_host, wait_max=10, n_samples=epoch_samples, verbose=False)

    # ---- Start Game client
    if connect_to_game:
        while not game_client.is_connected():
            game_client.connect()
        log("Connected to game server!")

    # ---- Start LSL client
    log("Connecting to LSL server...")
    lsl_client.start()  # Trows RuntimeError if not connected

    # ----- Class / callback table
    # Labels are ['left', 'right', 'up', 'down', 'eyeblink', 'nothing']
    action_dict = {
        0: game_client.move_left,
        1: game_client.move_right,
        2: game_client.move_up,
        3: game_client.move_down,
    }

    labels = ['left', 'right', 'up', 'down', 'eyeblink', 'nothing']

    # ---- Register epoch received routine
    def epoch_received(epoch, epoch_index):
        print("Received: \n {}".format(epoch))
        epoch_samples = len(epoch.times)

        markers = {'+': 0000,
                   'left': 1001,
                   'right': 2001,
                   'up': 3001,
                   'down': 4001,
                   'eyeblink': 7000,
                   'nothing': 7001,
                   'start_pause': 8001,
                   'end_pause': 8002,
                   'end_trial': 9999}

        if mock_server:
            event = dt.get_stim_events(epoch)
            if 1 in event:
                print("Event: left")
            if 2 in event:
                print("Event: right")
            if 3 in event:
                print("Event: up")
            if 4 in event:
                print("Event: down")

        # Remove any stim-channels as the classifier does not expect them.
        epoch = dt.drop_stim_channels(epoch)

        epoch = epoch.apply_baseline((None, 0.05))
        epoch = epoch.filter(1, 40)
        data = transform_data.transform_data(epochs=epoch)

        pred_probdens = classifier.predict(data)
        pred_class_indx = np.argmax(pred_probdens)
        pred_confidence = pred_probdens[0, pred_class_indx]

        print("Epochs since start: {}".format(epoch_index))
        print("Guess: {0} {1}   conf: {2}".format(pred_class_indx, labels[pred_class_indx], pred_confidence))
        if mock_server:
            print("Actual: {0}".format(event))

        print("\n")

        """
        epoch_data = epoch.get_data()[0]
        plt.plot(epoch_index * epoch_samples + np.arange(0, epoch_samples), epoch_data.T * 1e6)
        plt.draw()
        plt.pause(0.0001)
        """

    lsl_client.register_epoch_received_callback(epoch_received)

    # ---- Exit stuff down here -->

    # Wait for console input.
    while True:
        log("Type 'quit' to exit program")
        x = input(">")

        if x == "quit":
            break

    # Stop LSL and game clients.
    if connect_to_game:
        game_client.disconnect()

    lsl_client.stop()

    if mock_server:
        server.stop()
