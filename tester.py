import process_data
import mne
import matplotlib.pyplot as plt


[epochs, raw] = process_data.process_data()

print(epochs)
print(raw)

mne.viz.plot_raw(raw)
plt.show()


"""
trial_list = ["Erik_1", "Erik_2", "Erik_3"]
_, raw_list = process_data.get_data(trial_list)

raw = raw_list[0] # For testing

raw = process_data._add_annotations_of_nothing(raw, trial_list[0])
raw = process_data._add_annotations_of_eyes(raw, trial_list[0])
events, _ = mne.events_from_annotations(raw)

print(events)

print(range(len(events)))
"""