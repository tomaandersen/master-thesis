from multiprocessing.connection import Listener
from multiprocessing import Queue
from threading import Thread


class IPCServer:

    def __init__(self, address, authkey, callback=None):
        self._receive_queue = Queue()
        self._address = address
        self._authkey = authkey
        self._callback = callback
        self._connection = None
        self._running = False

    def start(self):
        self._running = True

        IPCServer._log("Listening for clients...")
        server = Listener(address=self._address, authkey=self._authkey)
        self._connection = server.accept()
        IPCServer._log("Client accepted.")

        p1 = Thread(target=self._receive_thread)
        p1.start()
        IPCServer._log("Receive thread started")

    def shutdown(self):
        pass

    def send_message(self, msg):
        self._connection.send(msg)

    def get_message(self, blocking=False, timeout=None):
        try:
            return self._receive_queue.get(block=blocking, timeout=timeout)
        except:
            return None

    def _receive_thread(self):
        while self._running:
            msg = self._connection.recv()
            self._receive_queue.put(msg)
            self._log(msg)

            if self._callback is not None:
                self._callback()

    @staticmethod
    def _log(msg):
        print("[IPCServer] {}".format(msg))