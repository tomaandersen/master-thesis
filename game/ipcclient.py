from multiprocessing.connection import Client
from multiprocessing import Queue
from multiprocessing.context import AuthenticationError
from threading import Thread


class IPCClient:

    def __init__(self, address, authkey, callback=None):
        self._receive_queue = Queue()
        self._address = address
        self._authkey = authkey
        self._callback = callback
        self._isconnected = False
        self._connection = None

    def connect(self):
        IPCClient._log("Connection to server...")
        try:
            self._connection = Client(address=self._address, authkey=self._authkey)
        except AuthenticationError:
            self._log("Authentication error.")
            return
        except Exception:
            self._log("An error occurred.")
            raise(Exception)
            return

        self._isconnected = True

        p1 = Thread(target=self._receive_thread)
        p1.daemon = True
        p1.start()
        IPCClient._log("Receive thread started")

    def disconnect(self):
        raise Exception("Function not jet implemented")

    def is_connected(self):
        return self._isconnected

    def get_message(self, blocking=False, timeout=None):
        return self._receive_queue.get(block=blocking, timeout=timeout)

    # TODO: if this method takes to long to execute => do this in another thread
    # TODO: ...and create a Queue where messages are waiting to be sent.
    def send_message(self, msg):
        self._connection.send(msg)

    def _receive_thread(self):
        while self._isconnected:
            msg = self._connection.recv()
            self._receive_queue.put(msg)
            self._log(msg)

            if self._callback is not None:
                self._callback()

    @staticmethod
    def _log(msg):
        print("[IPCClient] {}".format(msg))