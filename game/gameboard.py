import numpy as np
import random


class GameBoard:
    NOTHING = 0
    SNAKE_HEAD = 1
    SNAKE_BODY = 2
    APPLE = 10
    WALL = -1

    UP = 0
    RIGHT = 1
    DOWN = 2
    LEFT = 3

    def __init__(self, game_size):
        game_size = tuple(np.array(game_size).tolist())

        self._game_size = game_size
        self._GAME_SIZE = game_size
        self._board = np.ones(game_size) * GameBoard.NOTHING
        self._player_position = None

    def get_width(self):
        return self._game_size[0]

    def get_height(self):
        return self._game_size[1]

    def clear_board(self):
        self._board = np.ones(self._game_size) * GameBoard.NOTHING
        self._player_position = None

    def set_tile(self, game_object_id, position):
        position = tuple(np.array(position).tolist())
        if self._is_inside_board(position):
            current_game_object_id = self._board[position]

            # Delete player if current position is occupied by player.
            if current_game_object_id == GameBoard.SNAKE_HEAD:
                self._player_position = None

            if game_object_id == GameBoard.SNAKE_HEAD:
                if self._player_position is not None:
                    self._log("One player already on game board. New player not added")
                else:
                    self._player_position = position

            self._board[position] = game_object_id

        else:
            self._log("Outside map, game object not placed.")

    def get_tile(self, position):
        position = tuple(np.array(position).tolist())
        if self._is_inside_board(position):
            return self._board[position]
        else:
            return None

    def move_player(self, player_direction):
        if self._player_position is None:
            self._log("Could not move player as no player is added to the game board.")
            return

        new_player_position = GameBoard._get_new_position(
            self._player_position, player_direction)

        # If new position is empty or contains apple, and is inside board.
        if self._is_traversable_position(new_player_position):
            self._board[new_player_position] = GameBoard.SNAKE_HEAD
            self._board[self._player_position] = GameBoard.NOTHING
            self._player_position = new_player_position

    def _is_traversable_position(self, position):
        position = tuple(np.array(position).tolist())
        if not self._is_inside_board(position):
            return False
        else:
            return self._board[position] == GameBoard.NOTHING or self._board[position] == GameBoard.APPLE

    def _is_inside_board(self, position):
        position = tuple(np.array(position).tolist())
        x = position[0]
        y = position[1]
        return 0 <= x < self._GAME_SIZE[0] and 0 <= y < self._GAME_SIZE[1]

    def _log(self, msg):
        print("[GameBoard] {}".format(msg))

    def get_random_traversable_position(self):
        trav_pos = np.where(self._board == GameBoard.NOTHING)
        numb_free_positions = len(trav_pos[0])

        # If any position is available
        if numb_free_positions > 0:
            rand_index = random.choice(np.arange(0, numb_free_positions))

            rand_x = trav_pos[0][rand_index]
            rand_y = trav_pos[1][rand_index]
            return rand_x, rand_y
        else:
            return None

    def _set_border(self, tile_type):
        """
        Set the tiles at the map border to a specific tile type (e.g. walls)
        """
        self._board[:, 0] = tile_type
        self._board[:, -1] = tile_type
        self._board[0] = tile_type
        self._board[-1] = tile_type

    @staticmethod
    def _get_new_position(position, direction):
        position = tuple(np.array(position).tolist())
        x = position[0]
        y = position[1]

        if direction == GameBoard.UP:
            return x, y - 1
        elif direction == GameBoard.RIGHT:
            return x + 1, y
        elif direction == GameBoard.DOWN:
            return x, y + 1
        elif direction == GameBoard.LEFT:
            return x - 1, y
