import pygame
import numpy as np
from gameboard import GameBoard
from ipcserver import IPCServer
from argparse import ArgumentParser
import threading
import shlex


class Game:
    ## COLOR STUFF
    COLOR_WHITE = (255, 255, 255)
    COLOR_BLACK = (0, 0, 0)
    COLOR_ORANGE = (255, 165, 0)
    COLOR_GRID = COLOR_ORANGE
    COLOR_ERROR = (100, 100, 255)
    COLOR_NOTHING = (0, 0, 0)
    COLOR_SNAKE_HEAD = (0, 255, 0)
    COLOR_SNAKE_BODY = (30, 200, 30)
    COLOR_APPLE = (255, 0, 0)
    COLOR_WALL = (100, 100, 100)

    ## GAME STATES
    GAME_STATE_INIT = 0
    GAME_STATE_ACTIVE = 1
    GAME_STATE_EXIT = -1

    def __init__(self, server_authkey=b'secret', server_address=('localhost', 16000)):
        # Screen
        self._target_fps = 60
        self._game_state = Game.GAME_STATE_INIT

        # Two arguments parsers. One when the server is waiting for startup and one that is used after
        # the initialization phase. We could use one, but here I use two.
        self._parser_init = None
        self._parser_active = None
        self._setup_parser()

        # Create setup wait event.
        self._setup_done_event = threading.Event()

        self._server_address = server_address
        self._server_authkey = server_authkey
        self._server = IPCServer(address=server_address,
                                 authkey=server_authkey,
                                 callback=self._message_received)
        self._server.start()

        # Default values, will be changed in other thread. Good practice? I don't know.
        self._game_size = [10, 10]
        self.WINDOW_SIZE = [400, 400]
        self.WINDOW_TITLE = "Default"

        # Wait here til setup is done.
        self._setup_done_event.wait()
        Game._log("Thread lock unlocked. Proceeding to start pygame.")

        # GameBoard
        self._game_board = GameBoard(self._game_size)

        # Monitor and screen.
        pygame.init()
        self._monitor_info = pygame.display.Info()
        self.MONITOR_SIZE = [self._monitor_info.current_w, self._monitor_info.current_h]

        # TODO: Remove this, or not. Think about it.
        # Smart window size. Set window size so that tile height = tile width
        if np.any(np.array(self.WINDOW_SIZE) <= 0):
            self.WINDOW_SIZE = self._get_smart_window_size()


        # Tile size
        self._tile_pix_size = Game._get_tile_size(self.WINDOW_SIZE, self._game_size)

    def _setup_parser(self):
        self._parser_init = ArgumentParser()
        self._parser_active = ArgumentParser()

        self._parser_init.add_argument('--gamesize', nargs=2, type=int)
        self._parser_init.add_argument('--wintitle', nargs=1, type=str)
        self._parser_init.add_argument('--winsize', nargs=2, type=int)

        self._parser_active.add_argument('--move', nargs=1, type=int)
        self._parser_active.add_argument('--set', nargs=3, type=str)
        self._parser_active.add_argument('--quit', action='store_true')
        self._parser_active.add_argument('--clear', action='store_true')

    @staticmethod
    def _log(msg):
        print("[Game] {}".format(msg))

    def _get_smart_window_size(self):
        """
        Experimental. Used to automatically set screen size.
        """
        from math import floor

        # Tile size (x, y) if the whole monitor was used
        rxy = Game._get_tile_size(self.MONITOR_SIZE, self._game_size)
        rx = rxy[0]
        ry = rxy[1]

        # Number of tiles on screen
        nx = self._game_size[0]
        ny = self._game_size[1]

        margin = np.array([30, 100])
        if rx > ry:
            return np.array([floor(nx * ry), self.MONITOR_SIZE[1]]) - margin
        else:
            return np.array([self.MONITOR_SIZE[0], floor(ny * rx)]) - margin

    @staticmethod
    def _get_tile_size(window_size, game_size):
        return np.round(np.divide(window_size, game_size))

    def _message_received(self):
        msg = self._server.get_message(blocking=True, timeout=0.015)
        if msg is None: return
        command_args = shlex.split(msg)

        # The incoming messages are parsed differently depending on the game state.
        if self._game_state == Game.GAME_STATE_INIT:
            p = vars(self._parser_init.parse_args(command_args))

            self._game_size = p.get("gamesize") or self._game_size
            self.WINDOW_TITLE = p.get("wintitle")[0] or self.WINDOW_TITLE
            self.WINDOW_SIZE = p.get("winsize") or self.WINDOW_SIZE

            self._setup_done_event.set()    # Setup is done. Set flag.
            self._game_state = Game.GAME_STATE_ACTIVE
            Game._log("Setup commands parsed. Setup completed.")

        else:
            p = vars(self._parser_active.parse_args(command_args))

            if p.get("quit"):
                self.exit_game()

            if p.get("clear"):
                self._game_board.clear_board()

            move_cmd = p.get("move")
            if move_cmd is not None:
                self._game_board.move_player(move_cmd[0])

            set_cmd = p.get("set")
            if set_cmd is not None:
                pos = (int(set_cmd[1]), int(set_cmd[2]))
                tile_type = int(set_cmd[0])
                self._game_board.set_tile(tile_type, pos)


    def play(self):

        # Init game stuff.
        self._init_game()

        # Create frame rate locker.
        clock = pygame.time.Clock()

        # Main game loop here
        while self._game_state == Game.GAME_STATE_ACTIVE:
            # Stabilizes the fps.
            clock.tick(self._target_fps)

            # Handle keyboard and mouse events here:
            self._event_handle()

            # Fill background:
            self._display.fill(Game.COLOR_BLACK)

            # Draw objects:
            self._draw_all()
            # pygame.draw.circle(self._display, Game.COLOR_ORANGE, (0, 0), 100)
            pygame.display.update()

        pygame.quit()
        quit()

    def _draw_all(self):
        self._draw_board()
        self._draw_grid()

    def _draw_board(self):
        # Draw vertical grid
        for xi in range(0, self._game_size[0]):
            for yi in range(0, self._game_size[1]):
                tile_type = self._game_board.get_tile((xi, yi))

                if tile_type == GameBoard.NOTHING:
                    color = Game.COLOR_NOTHING
                elif tile_type == GameBoard.SNAKE_HEAD:
                    color = Game.COLOR_SNAKE_HEAD
                elif tile_type == GameBoard.SNAKE_BODY:
                    color = Game.COLOR_SNAKE_BODY
                elif tile_type == GameBoard.APPLE:
                    color = Game.COLOR_APPLE
                elif tile_type == GameBoard.WALL:
                    color = Game.COLOR_WALL
                else:
                    color = Game.COLOR_ERROR

                rect = pygame.Rect(xi * self._tile_pix_size[0],
                                   yi * self._tile_pix_size[1],
                                   self._tile_pix_size[0],
                                   self._tile_pix_size[1])

                pygame.draw.rect(self._display, color, rect, width=0)

    def _draw_grid(self):
        # Draw vertical grid
        for xi in range(1, self._game_size[0]):
            pygame.draw.line(self._display, Game.COLOR_GRID,
                             (xi * self._tile_pix_size[0], 0),
                             (xi * self._tile_pix_size[0], self.WINDOW_SIZE[1]), 1)

        for yi in range(1, self._game_size[1]):
            pygame.draw.line(self._display, Game.COLOR_GRID,
                             (0, yi * self._tile_pix_size[1]),
                             (self.WINDOW_SIZE[0], yi * self._tile_pix_size[1]), 1)

    def _event_handle(self):
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                self.exit_game()

    def _init_game(self):
        self._display = pygame.display.set_mode(self.WINDOW_SIZE)
        pygame.display.set_caption(self.WINDOW_TITLE)

    def exit_game(self):
        self._server.shutdown()
        self._game_state = Game.GAME_STATE_EXIT


if __name__ == '__main__':
    game = Game()
    game.play()
