from game.gameboard import GameBoard
from game.ipcclient import IPCClient
from time import sleep
import numpy as np

class GameClient:
    """
    Class that simplifies the connection between the game server and client endpoint.
    """

    def __init__(self, game_size, address=('localhost', 16000), authkey=b'secret'):
        """
        :param game_size: Number of tiles in the width and height dimension
        :param address: Address to remote game server
        :param authkey: Authentication key. The server must use the same key.
        """
        self._game_board = GameBoard(game_size=game_size)
        self._conn = IPCClient(address, authkey)

    def connect(self):
        """
        Tries to connect to remote server specified by the address. Sends setup commands.
        :return: None
        """

        # Tries to connect every 4 seconds:
        while not self._conn.is_connected():
            self._conn.connect()
            sleep(4)

        # Send setup commands (i.e. size of screen etc)
        self._conn.send_message('--wintitle " " --gamesize {0} {1} --winsize -1 -1'.format(
            self._game_board.get_width(), self._game_board.get_height()))

    def disconnect(self):
        """
        Attempt to disconnect from game server.
        """
        self._conn.disconnect()

    def create_player(self, position):
        """
        Creates a player tile at position. If player already exist in game nothing happens.
        Args:
            position: Tuple object that specifies position.
        """
        self._create(GameBoard.SNAKE_HEAD, position)

    def create_apple(self, position):
        """
        Creates an apple tile at position. Removes player if player is located at specified position.
        Args:
            position: Tuple object that specifies position.
        """
        self._create(GameBoard.APPLE, position)

    def create_wall(self, position):
        """
        Creates a wall tile at position. Removes player if player is located at specified position.
        Args:
            position: Tuple object that specifies position.
        """
        self._create(GameBoard.WALL, position)

    def move_up(self):
        """
        Moves the player up. If no player is added to the game nothing will happen
        """
        self._move(GameBoard.UP)

    def move_right(self):
        """
        Moves the player to the right. If no player is added to the game nothing will happen
        """
        self._move(GameBoard.RIGHT)

    def move_down(self):
        """
        Moves the player down. If no player is added to the game nothing will happen
        """
        self._move(GameBoard.DOWN)

    def move_left(self):
        """
        Moves the player to the left. If no player is added to the game nothing will happen
        """
        self._move(GameBoard.LEFT)

    def clear_board(self):
        """
        Clears the board, i.e. sets every tile to "nothing"
        """
        self._conn.send_message('--clear')
        self._game_board.clear_board()

    def _move(self, direction):
        self._conn.send_message('--move {}'.format(direction))
        self._game_board.move_player(direction)

    def _create(self, tile_type, position):
        """ Send commands to the remote game server, and update the local copy of the game
        board.
        """
        position = np.array(position).tolist()
        self._conn.send_message('--set {0} {1} {2}'.format(tile_type, position[0], position[1]))
        self._game_board.set_tile(tile_type, position)

