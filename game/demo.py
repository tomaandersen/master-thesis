from game_client import GameClient
import numpy as np

if __name__ == '__main__':

    game_size = (10, 10)
    gc = GameClient(game_size)
    gc.connect()

    for x in range(0, game_size[0]):
        gc.create_wall([x, 0])
        gc.create_wall([x, 9])

    gc.create_wall((4, 5))
    gc.create_wall((6, 8))
    gc.create_wall((7, 8))
    gc.create_wall((5, 5))

    gc.create_apple((4, 6))
    gc.create_player((4, 4))

    while True:
        input("Press enter to move randomly")
        dir = int(np.round(np.random.rand(1)*3)[0])
        print(dir)
        gc._move(dir)

    gc.disconnect()

