import mne
import process_data
import matplotlib.pyplot as plt
import transform_data

trial_list = "Erik_3"
trials_resource_path = "./Trials"

epochs, raw = process_data.process_data_add_nothing_and_eyeblinks(trial_list)

# Convert Annotation type to event array.
events = mne.events_from_annotations(raw)
event_id = events[1]
events = events[0]

epochs_labels = ['left', 'right', 'up', 'down', 'eyeblink', 'nothing']
start_time = 0.15
end_time = 0.55
win_size = 0.3
time_shift = 0.02

data, labels = transform_data.transform_all_types_of_epochs(
    epochs, epochs_labels, start_time=start_time, end_time=end_time, win_size=win_size, time_shift=time_shift)


print(data)

if False:
    event_dict = {'+': 1,
               'left': 2,
               'right': 3,
               'up': 4,
               'down': 5,
               'eyeblink': 6,
               'nothing': 7,
               'start_pause': 8,
               'end_pause': 9,
               'end_trial': 10}

    mne.viz.plot_raw(raw, events=events, event_id=event_dict, start=15, duration=10, n_channels=24)
    mne.viz.plot_events(events, event_id=event_dict)
    mne.viz.plot_epochs(epochs, n_epochs=5, n_channels=24, events=events, event_id=event_dict)
    plt.show()
